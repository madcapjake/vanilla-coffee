observe = (initVal) ->
    listeners = []

    value = initVal

    notify = (newVal) -> listen newVal for listen in listeners 
    
    accessor = (newVal) ->
        if arguments.length and newVal isnt initVal
            value = newVal
            notify newVal
        value
            
    accessor.subscribe = (listener) -> listeners.push listener
    
    accessor

compute = (dependencies, compVal) ->
    value = observe compVal()
    
    listener = -> value compVal()
    
    dep.subscribe listener for dep in dependencies
    
    getter = -> value()
    getter.subscribe = value.subscribe
    
    getter
    
bind = (input, observable) ->
    initial = observable()
    input.value = initial
    observable.subscribe -> input.value = observable()
    
    convert = switch
        when typeof initial is 'number'
            (n) -> if isNaN(n = parseFloat n) then 0 else n
        else
            (n) -> n
    input.addEventListener 'input', -> observable convert input.value

t = (tag, attrsOrChildren, children) ->
    appendAll = (kids) -> el.appendChild(k) for k in kids
    el = document.createElement tag
    switch typeof attrsOrChildren
        when 'object'
            if attrsOrChildren.nodeName?
                el.appendChild attrsOrChildren
            else
                if attrsOrChildren.bind?
                    bind el, attrsOrChildren.bind
                    delete attrsOrChildren.bind
                el.setAttribute(k, v) for k, v of attrsOrChildren
        when 'array'  then appendAll attrsOrChildren
        else el.appendChild String attrsOrChildren
    switch typeof children
        when 'object' then el.appendChild children
        when 'array' then appendAll children
    return el
