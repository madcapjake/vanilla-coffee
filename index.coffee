a = observe 3
b = observe 2
c = compute [a, b], -> a() + b()  

inputA = t 'input', type: 'text', bind: a
document.body.appendChild inputA

inputB = t 'input', type: 'text', bind: b
document.body.appendChild inputB

inputC = t 'input', type: 'text', bind: c
document.body.appendChild inputC
