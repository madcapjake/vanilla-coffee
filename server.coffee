http = require 'http'
fs = require 'fs'
path = require 'path'

port = 8125

server = http.createServer (request, response) ->
    console.log 'request ', request.url

    filePath = '.' + request.url
    if filePath is './' then filePath = './index.html'

    extname = path.extname filePath

    mimeTypes =
        '.html' : 'text/html'
        '.js'   : 'text/javascript'
        '.css'  : 'text/css'
        '.json' : 'application/json'
        '.png'  : 'image/png'
        '.jpg'  : 'image/jpg'
        '.gif'  : 'image/gif'
        '.wav'  : 'audio/wav'
        '.mp4'  : 'video/mp4'
        '.woff' : 'application/font-woff'
        '.ttf'  : 'application/font-ttf'
        '.eot'  : 'application/vnd.ms-fontobject'
        '.otf'  : 'application/font-otf'
        '.svg'  : 'application/image/svg+xml'

    contentType = mimeTypes[extname] ? 'application/octet-stream'

    fs.readFile filePath, (error, content) ->
        if error
            if error.code is 'ENOENT'
                notFoundPage = './404.html'
                fs.readFile notFoundPage, (error, content) ->
                    response.writeHead 404, 'Content-Type': mimeTypes[path.extname notFoundPage]
                    response.end content, 'utf-8'
            else
                response.writeHead 500
                response.end "Sorry, check with the site admin for error: #{error.code} ..\n"
                response.end()
        else
            response.writeHead 200, 'Content-Type': contentType
            response.end content, 'utf-8'

server.listen port
console.log "Server running at http://127.0.0.1:#{port}/"
